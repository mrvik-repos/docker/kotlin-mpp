FROM docker.io/archlinux:base

RUN pacman -Syu --noconfirm --needed base{,-devel} git jdk11-openjdk \
       chromium && \
       pacman -Sc --noconfirm

ADD launch-chromium /usr/bin/launch-chromium
RUN chmod 755 /usr/bin/launch-chromium

ENV CHROME_BIN=/usr/bin/launch-chromium
ENV CHROMIUM_BIN=/usr/bin/launch-chromium
ENV CHROME_WRAPPED_BINARY=/usr/bin/chromium
